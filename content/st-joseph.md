Title: Rétrospective années scoutes
Date: 2024-09-13
Category: Scout
Lang: fr

Ça y est, j'ai raccroché ma chemise de chef. Six ans déjà.
Six années toutes différentes, toujours une nouvelle aventure, de nouveaux projets.

La première année, à Quimper, j'arrive au milieu de l'année après la démission des chefs précédents qui ne s'entendaient plus entre eux.
Seul chef, je me fais aider par les parents l'année, puis nous partons en camp accompagné au Jamboree Connecte.

L'année suivante, nous partons en camp après une année sans sortie, et passons un camp sans histoire à la base de Kerjust.

En troisième année, nous allons à la Mouline, près de Bordeaux, et nous découvrons les joies des retards de train (1h de retard sur notre premier train, au final 2h à l'arrivée).
C'était ma première direction de camp.

Pour ma dernière année à Quimper, nous tentons de faire un jumelage, qui n'aboutira pas.
Nous partons en camp en Finistère, avec une co-chef directrice, et nous ferons du kayak sur l'Aulne.

Je déménage à Nantes, et y trouve [le groupe St-Joseph](https://sites.sgdf.fr/saint-joseph-nantes/), où je suis bien accueilli par la maitrise.
Nous partons à Vernantes, proche de Saumur et nous y tournons un film.

Pour la dernière année, nous avons construit des radeaux au chateau de Perennou, et avons navigué sur l'Odet.
