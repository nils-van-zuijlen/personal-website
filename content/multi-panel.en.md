Title: LED Multi-panel
Date: 2024-01-06
Modified: 2024-10-10
Category: Electronics
Lang: en
Translation: true


I was inspired by the T^2 Tiles project on [Hackaday](https://hackaday.io/project/184194-t2-tiles-gd0095), which aims to mount LED panels on arms allowing 6D movements (3 rotations and 3 translations).

Like him, I found P2.5 beveled panels, but their price and the complexity in controlling them led me to branch out into tests with smaller panels that were easier to control.
So I went for 8x8 panels of WS2812 LEDs, which can be controlled using an already well supported protocol, and above all which doesn't require multiplexing or LED refresh from the microcontroller.

As for movement, I've chosen to reduce it to two dimensions (2 rotations) with 9g miniservos.

As I'm learning to use [Rust Embedded](https://github.com/rust-embedded/awesome-embedded-rust), and I have Raspberry Pi Pico, I chose to use them for control.
As I have experience of DMX control with [QLC+](https://www.qlcplus.org), I chose to use this protocol.

A module therefore is made of 2 servos and 64 RGB LEDs, i.e. 194 control variables.
A DMX universe can only control two of them.
I want to be able to control 16 (4x4 modules), i.e. 8 DMX universes.
Since my current DMX setup uses ArtNet, I think it's more interesting to use ArtNet directly for the panel, since a single IP can receive 4 DMX universes directly.
So I only need one controller for 8 modules, if I have one IP per controller.

For ArtNet communication, I need to communicate in UDP, via Ethernet for reliability.
The Embassy framework natively supports the W5500 module, which easily enables UDP communication via an SPI bus.

If I connect all the servos directly to the RPi Pico, I won't have any pins left for status LEDs or configuration switches.
So I'm going to use a PCA9685 board, which gives me 16 PWM outputs on a single I2C bus, and is therefore perfectly suited to 8 modules of 2 servos each.

On the mechanical side, I tried to make the pivot myself using 3D printing, but I couldn't keep the alignment between the panel and the base (V1 of the 3D model), so I'm going to use a commercial gimbal coupler.

My code: [GitLab](https://gitlab.com/nils-van-zuijlen/multi-panel)

The 3D files: [OnShape](https://cad.onshape.com/documents/1b8e32f48dfb0850993f6023/w/7d9bf3ffa5bef09d6a478fa0/e/81e05433c2a7dcbc47e44a87?renderMode=0&uiState=65995ca18b075a5d5f1beace)
