Title: A retrospective of my scouting years
Date: 2024-09-13
Category: Scout
Lang: en
Translation: true
Slug: retrospective-annees-scoutes

That's it, I've hung up my scout leader shirt. Six years already.
Six very different years, always a new adventure, new projects.

The first year, in Quimper, I arrived in the middle of the year after the previous leaders resigned because they couldn't get along with each other.
As the only leader, I got help from the parents during the year, and then we went on a camp to the Connecte Jamboree.

The following year, we went to camp after a year without any outings, and had an uneventful camp at the Kerjust base.

In our third year, we went to La Mouline, near Bordeaux, and discovered the joys of train delays (1 hour late on our first train, 2 hours on arrival).
It was my first camp management.

For my last year in Quimper, we tried to set up a twinning program, but it didn't work out.
We went to camp in Finistère, with a co-director, and kayaked on the Aulne.

I move to Nantes, and find [the St-Joseph group](https://sites.sgdf.fr/saint-joseph-nantes/), where I am well received by the other leaders.
We went to Vernantes, near Saumur, and shot a film there.

For the last year, we built rafts at the chateau de Perennou, and sailed on the Odet.
