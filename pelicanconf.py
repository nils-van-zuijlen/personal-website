AUTHOR = 'Nils Van Zuijlen'
SITENAME = 'Nils Van Zuijlen'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = 'fr'
LOCALE = ('en_US.utf8', 'fr_FR.utf8')

THEME = "alchemy"

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
FOOTER_LINKS = (('HedgeDoc', 'https://pads.bachelay.eu'),)

# Social widget
SOCIAL = (('Mastodon: @amael@social.linux.pizza', 'https://social.linux.pizza/@amael'),)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
