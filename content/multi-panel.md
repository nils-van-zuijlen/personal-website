Title: LED Multi-panel
Date: 2024-01-06
Modified: 2024-10-10
Category: Électronique
Lang: fr


J'ai été inspiré par le projet T^2 Tiles sur [Hackaday](https://hackaday.io/project/184194-t2-tiles-gd0095) qui souhaite monter des panneaux LED sur des bras permettant des mouvements en 6D (3 rotations et 3 translations).

Comme lui, j'ai trouvé des panneaux P2.5 biseautés, mais leur prix et la difficulté de leur contrôle m'ont fait bifurquer vers des tests avec des panneaux plus petits et plus simples à contrôler.
J'ai donc pris des panneaux 8x8 de LEDs WS2812, qui se contrôlent avec un protocole déjà bien supporté par des librairies, et surtout qui ne nécessite pas de faire de multiplexage ni de rafraîchissement des LEDs depuis le microcontrôleur.

Pour ce qui est du mouvement, j'ai choisi de le réduire à deux dimensions (2 rotations) avec des miniservos 9g.

Comme je suis en train d'apprendre à utiliser [Rust Embedded](https://github.com/rust-embedded/awesome-embedded-rust), et que j'ai des Raspberry Pi Pico, j'ai choisi de les utiliser pour le contrôle.
Comme j'ai de l'expérience avec le contrôle en DMX avec [QLC+](https://www.qlcplus.org), j'ai choisi d'utiliser ce protocole.

Un module comprend donc 2 servos et 64 LEDs RGB, soit 194 variables de contrôle.
Un univers DMX ne peut donc en contrôler que deux.
Or je veux pouvoir en contrôler 16 (4x4 modules), soit 8 univers DMX.
Vu que mon setup actuel pour avoir du DMX passe par de l'ArtNet, je pense qu'utiliser directement ArtNet pour le panneau est plus intéressant, puisqu'une IP peut recevoir 4 univers DMX directement.
Il me suffit donc d'avoir un contrôleur pour 8 modules, si j'ai une IP par contrôleur.

Pour la communication ArtNet, j'ai besoin de communiquer en UDP, via de l'Ethernet pour la fiabilité.
Le framework Embassy supporte nativement le module W5500, qui permet facilement la communication UDP via un bus SPI.

Si je branche tous les servos directement sur le RPi Pico, il ne me restera plus de pins disponibles pour des leds de statut ou des switchs de configuration.
Je vais donc utiliser une carte PCA9685 qui me donne 16 sorties en PWM avec un seul bus I2C, et convient donc parfaitement pour 8 modules de 2 servos chacun.

Côté mécanique, j'ai essayé de réaliser le pivot moi même en impression 3D, mais je n'ai pas réussi à conserver l'alignement entre le panneau et la base (V1 du modèle 3D), donc je vais utiliser un coupleur de cardan du commerce.

Mon code: [GitLab](https://gitlab.com/nils-van-zuijlen/multi-panel)

Les fichiers 3D: [OnShape](https://cad.onshape.com/documents/1b8e32f48dfb0850993f6023/w/7d9bf3ffa5bef09d6a478fa0/e/81e05433c2a7dcbc47e44a87?renderMode=0&uiState=65995ca18b075a5d5f1beace)
